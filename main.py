
def imprime_ola_mundo(texto):
    """
    Imprime Hello World no console do usuário

    >>> imprime_ola_mundo("Hello World")
    Hello World

    :return: str
    """
    return texto

if __name__ == '__main__':
    print(imprime_ola_mundo("Ola mundo"))